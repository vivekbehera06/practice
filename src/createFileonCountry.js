const fs = require("fs");

function readFile(filename) {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, "utf8", (err, data) => {
      if (err) {
        reject(err);
      } else {
        let obj = {};
        obj = JSON.parse(data).reduce((res, person) => {
          if (res.hasOwnProperty(person.country)) {
            res[person.country]++;
          } else {
            res[person.country] = 1;
          }
          return res;
        }, {});

        resolve(obj);
      }
    });
  });
}

readFile("/home/vivek/cohort-15.1/NodeJS/FileSystem/data/data1.json")
  .then((data) => {
    fs.writeFile(
      "/home/vivek/cohort-15.1/NodeJS/FileSystem/src/output/o1.json",
      JSON.stringify(data),
      (err) => {
        if (err) {
          console.error(err);
        }
      }
    );
  })
  .catch((err) => console.error(err));
