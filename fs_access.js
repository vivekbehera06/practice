const fs = require('fs');

console.log("Giving only read permission to the user");
fs.chmodSync("./test.txt", fs.constants.S_IRUSR);

fs.access("./test.txt", fs.constants.R_OK, (err) => {
    console.log("Checking permission for reading the file");
    if(err){
        console.error('No Read access')
    }else{
        console.log('File can be read');
    }
});

fs.access("./test.txt", fs.constants.R_OK | fs.constants.W_OK, (err) => {
    console.log("Checking permission for reading and writing the file");

    if(err){
        console.error('No Read and Write access');
    }else{
        console.log('File can be read and write');
    }
})

console.log('=====================================================');


fs.access("./test.txt", fs.constants.F_OK, (err) => {
    console.log("Check if file exists");

    if(err){
        console.error('File does not exist');
    }else{
        console.log('File does exist');
    }
})