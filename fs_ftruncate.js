const fs = require("fs");

console.log("Contents of file before truncate:")
console.log(fs.readFileSync('./input.txt', 'utf8'));
  
// Get the file descriptor of the file
const fd = fs.openSync('./input.txt', 'r+');
  
fs.truncate(fd, 19, (err) => {
  if (err)
    console.log(err)
  else {
    console.log("Contents of file after truncate:")
    console.log(fs.readFileSync('./input.txt', 'utf8'));
  }
});

