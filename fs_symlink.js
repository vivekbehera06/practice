const fs = require('fs');

console.log("Contents of the file")
console.log(fs.readFileSync('./input.txt', 'utf8'));

fs.symlink("./input.txt", "./symlinkToFile.txt", (err) => {
    if(err) throw err;

    console.log('Symlink created');
    console.log('Contents of the symlink created: ');
    console.log(fs.readFileSync('symlinkToFile', 'utf-8'));
})