const fs = require('fs');

console.log("Current Directory Path: ", __dirname);
path1 = __dirname + '/../../';

fs.realpath(path1, function(err, resolve){
    if (err) throw err;
    console.log("One directory up resolved path is : ", resolve);
});

path2 = __dirname + '/../../../'

fs.realpath('./input.txt', {encoding: 'base64'}, function(err, resolve){
    if(err) throw err;
    console.log("Two directories up resolved path is : ", resolve);
});