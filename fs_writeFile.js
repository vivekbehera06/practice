const fs = require('fs');

// let path = './input.txt';
// let buffer = new Buffer.from('I live in Balasore');

// fs.open(path, 'a', (err, fd) => {
//     if(err) throw err;

//     fs.write(fd, buffer, 0, buffer.length, null, (err, writtenBytes) => {
//         if(err) throw err;
//         console.log(writtenBytes + ' characters are added');
//     });
// })


fs.readFile('./test.txt', 'utf8', (err, data) => {
    if(err) throw err;
    //console.log(data)
    let path = './input.txt';
    let buffer = new Buffer.from(data);

    fs.open(path, 'w', (err, fd) => {
        console.log(fd)
        if(err) throw err;

        fs.write(fd, buffer, 0, buffer.length, null, (err) => {
            if(err) throw err;

            console.log('file written successfully');
        })
    })
})